from cdserver.asgi.request import Request
from cdserver.core.router import UrlRouter
import cdserver
from cdserver.asgi.response import Http404
from cdserver.utils.module_loading import import_string


async def send_response(send, response):
    headers = [
        *response.header,
        *((b'Set-Cookie', c.output(header='').encode('utf-8')) for c in response.cookies.values()),
    ]
    await send({
        'type': 'http.response.start',
        'status': response.status_code,
        'headers': headers,
    })
    for chunk in response:
        await send({
            'type': 'http.response.body',
            'body': chunk,
            'more_body': True
        })
    await send({
        'type': 'http.response.body',
        'body': b'',
    })


class MiddlewareHandler(object):
    def __init__(self, middlewares):
        self.middleware_chain = []
        for _path in middlewares:
            print('load middleware', _path)
            middleware = import_string(_path)
            self.middleware_chain.append(middleware)

    def process_request(self, request):
        for middleware in self.middleware_chain:
            if hasattr(middleware, 'process_request'):
                middleware.process_request(request)

    def process_response(self, request, response):
        for middleware in self.middleware_chain:
            if hasattr(middleware, 'process_response'):
                middleware.process_response(request, response)


class ASGIHandler(object):
    def __init__(self, urls, settings):
        print('init')
        setattr(cdserver, 'settings', settings)
        self.middleware_handler = MiddlewareHandler(getattr(settings, 'MIDDLEWARES', []))
        self.url_router = UrlRouter(urls)
        print(self.url_router.routers)

    async def __call__(self, scope, receive, send):
        # asgi3 interface

        view, kwargs = self.url_router.get_url_view(scope['path'])
        if scope['type'] == 'websocket':
            if view:
                await view()(scope, receive, send)
            else:
                await send({"type": "websocket.close", "code": 404})

        #elif scope['type'] == 'http':
        #    request = await Request(scope)(receive)
        #    self.middleware_handler.process_request(request)
        #    response = view(request, **kwargs)
        #    await send_response(send, response)


