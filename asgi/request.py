from cdserver.utils import cookie
import json
import urllib.parse


class FormData(object):
    def __init__(self, stream, content_length, boundary, chunk_size=64):
        self.stream = stream
        self.content_length = content_length
        self.boundary = b'--'+boundary.encode('ascii') if isinstance(boundary, str) else boundary
        self._boundary_pos = 0
        self._cache = b''
        self.read_length = 0
        self._files = {}
        self._post = []
        self.chunk_size = chunk_size
        self._value = b""
        self.load_data()


    def load_data(self):
        _line = True
        while _line:
            _line = self.chunk()

        self.stream.close()

    def put_value(self, value):
        if not self._value:
            return

        values = self._value.split(b'\r\n')
        opts = values[1].split(b';')
        print(values[1], opts)

    def _boundary(self, line):
        _line = self._cache + line
        while True:
            find = _line.find(self.boundary)
            if find > -1:
                self._value += _line[:find]
                _line = _line[find + len(self.boundary):]
                self.put_value(self._value)
                print(find, self._value, _line)
                self._value = b''
            else:
                break
        self._cache = _line

    def chunk(self):
        """

        :return: readed, b'content'
        """
        if self.read_length >= self.content_length:
            return b''

        size = self.chunk_size
        if self.read_length + self.chunk_size > self.content_length:
            size = self.content_length - self.read_length

        _line = self.stream.read(size)
        self.read_length += size
        self._boundary(_line)
        return _line


class Request(object):

    async def __call__(self, receive):
        """
        Read and return the entire body from an incoming ASGI message.
        """
        self.body = b''

        more_body = True
        while more_body:
            message = await receive()
            self.body += message.get('body', b'')
            more_body = message.get('more_body', False)
        return self

    def __init__(self, scope):
        self.META = scope
        self.path = self.META.get('path', '/')
        self.headers = {}
        for k, v in self.META['headers']:
            k = k.decode().replace('-', '_')
            self.headers[k] = v.decode()


    @property
    def method(self):
        return self.META['method']

    @property
    def COOKIES(self):
        raw_cookie = get_str_from_wsgi(self.META, 'HTTP_COOKIE', '')
        return cookie.parse_cookie(raw_cookie)

    @property
    def POST(self):
        if hasattr(self, '_post_data'):
            return getattr(self, '_post_data')

        content_type = self.headers.get('content_type', '')

        if content_type == 'application/json':
            self._post_data = json.loads(self.body.decode('utf-8')) if len(self.body) else {}
            return self._post_data

        return {}

    @property
    def GET(self):
        if hasattr(self, '_get_data'):
            return self._get_data

        query_string = self.META['query_string']
        self._get_data = urllib.parse.parse_qs(query_string)
        return self._get_data



