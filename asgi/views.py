# asgi view
import asyncio
from concurrent.futures import CancelledError
from cdserver.core.layers import RedisLayer
from cdserver.asgi.exceptions import *
import functools


async def await_many_dispatch(consumer_callables, dispatch):
    """
    Given a set of consumer callables, awaits on them all and passes results
    from them to the dispatch awaitable as they come in.
    """
    # Start them all off as tasks
    loop = asyncio.get_event_loop()
    tasks = [
        loop.create_task(consumer_callable())
        for consumer_callable in consumer_callables
    ]
    try:
        while True:
            # Wait for any of them to complete
            await asyncio.wait(tasks, return_when=asyncio.FIRST_COMPLETED)
            # Find the completed one(s), yield results, and replace them
            for i, task in enumerate(tasks):
                if task.done():
                    result = task.result()
                    await dispatch(result)
                    tasks[i] = asyncio.ensure_future(consumer_callables[i]())

    finally:
        # Make sure we clean up tasks on exit
        for task in tasks:
            task.cancel()
            try:
                await task
            except CancelledError:
                pass


def get_handler_name(message):
    """
    Looks at a message, checks it has a sensible type, and returns the
    handler name for that type.
    """
    # Check message looks OK
    if "type" not in message:
        raise ValueError("Incoming message has no 'type' attribute")
    if message["type"].startswith("_"):
        raise ValueError("Malformed type in message (leading underscore)")
    # Extract type and replace . with _
    return message["type"].replace(".", "_")


class AsyncView:
    """
    Base consumer class. Implements the ASGI application spec, and adds on
    channel layer management and routing of events to named methods based
    on their type.
    """

    async def __call__(self, scope, receive, send):
        """
        Dispatches incoming messages to type-based handlers asynchronously.
        """
        # Initalize channel layer
        # Store send function
        self.scope = scope
        self.base_send = send
        self.layer = RedisLayer()
        self.channel_name = await self.layer.new_channel()
        self.channel_receive = functools.partial(
            self.layer.receive, self.channel_name
        )
        try:
            await await_many_dispatch([receive, self.channel_receive], self.dispatch)

        except StopView:
            pass


    async def dispatch(self, message):
        """
        Dispatches incoming messages to type-based handlers asynchronously.
        """
        # Get and execute the handler
        handler = getattr(self, get_handler_name(message), None)
        if handler:
            await handler(message)
        else:
            raise ValueError("No handler for message type %s" % message["type"])

    @classmethod
    def as_view(cls):
        return cls()

class WebSocketView(AsyncView):
    """
    Base consumer class. Implements the ASGI application spec, and adds on
    channel layer management and routing of events to named methods based
    on their type.
    """

    async def connect(self):
        await self.accept()

    async def accept(self, subprotocol=None):
        """
        Accepts an incoming socket
        """
        await self.base_send({"type": "websocket.accept", "subprotocol": subprotocol})

    async def close(self, code=None):
        """
        Closes the WebSocket from the server end
        """
        if code is not None and code is not True:
            await self.base_send({"type": "websocket.close", "code": code})
        else:
            await self.base_send({"type": "websocket.close"})

    async def receive(self, message):
        pass

    async def disconnect(self, code):
        pass

    async def send(self, text_data=None, bytes_data=None, close=False):
        """
        Sends a reply back down the WebSocket
        """
        if text_data is not None:
            await self.base_send({"type": "websocket.send", "text": text_data})
        elif bytes_data is not None:
            await self.base_send({"type": "websocket.send", "bytes": bytes_data})
        else:
            raise ValueError("You must pass one of bytes_data or text_data")
        if close:
            await self.close(close)

    async def websocket_connect(self, message):
        await self.connect()

    async def websocket_receive(self, message):
        """
        Called when a WebSocket frame is received. Decodes it and passes it
        to receive().
        """
        if 'text' in message:
            await self.receive(message['text'])
        else:
            await self.receive(message['bytes'])

    async def websocket_disconnect(self, message):
        """
        Called when a WebSocket connection is closed. Base level so you don't
        need to call super() all the time.
        """
        await self.disconnect(message["code"])
        raise StopView()


