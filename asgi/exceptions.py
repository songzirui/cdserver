class StopView(Exception):
    """
    Raised when a consumer wants to stop and close down its application instance.
    """

    pass
