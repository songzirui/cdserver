import datetime
import time
import json
from http.cookies import SimpleCookie


class Response(object):

    def __init__(self, content='',
                 header=(('Content-Type', 'text/html;charset=utf-8'), ),
                 status_code='200 OK'):

        if isinstance(content, str):
            content = content.encode('utf-8')

        self.status_code = status_code
        self.content = [content]
        self.header = list(header)
        self.cookies = {}

    def __iter__(self):
        return iter(self.content)


class JsonResponse(Response):

    def __init__(self,  content):
        if not isinstance(content, str):
            content = json.dumps(content)

        super().__init__(
            content=content,
            header=[('Content-Type', 'application/json;charset=utf-8'), ]
        )


class HttpResponse(Response):

    def __init__(self,  content):
        super().__init__(content=content)
        self.cookies = SimpleCookie()

    def set_cookie(self, key, value='', max_age=None, expires=None, path='/',
                   domain=None, secure=False, httponly=False):
        """
        Set a cookie.

        ``expires`` can be:
        - a string in the correct format,
        - a naive ``datetime.datetime`` object in UTC,
        - an aware ``datetime.datetime`` object in any time zone.
        If it is a ``datetime.datetime`` object then calculate ``max_age``.
        """
        self.cookies[key] = value
        if expires is not None:
            if isinstance(expires, datetime.datetime):
                delta = expires - expires.utcnow()
                delta = delta + datetime.timedelta(seconds=1)
                # Just set max_age - the max_age logic will set expires.
                expires = None
                max_age = max(0, delta.days * 86400 + delta.seconds)
            else:
                self.cookies[key]['expires'] = expires
        else:
            self.cookies[key]['expires'] = ''
        if max_age is not None:
            self.cookies[key]['max-age'] = max_age
            # IE requires expires, so set it if hasn't been already.
            # if not expires:
            #    self.cookies[key]['expires'] = http_date(time.time() + max_age)

        if path is not None:
            self.cookies[key]['path'] = path
        if domain is not None:
            self.cookies[key]['domain'] = domain
        if secure:
            self.cookies[key]['secure'] = True
        if httponly:
            self.cookies[key]['httponly'] = True


class FileResponse(object):

    def __init__(self, content='',
                 header=((b'Content-Type', b'text/html;charset=utf-8'), ),
                 status_code=200):

        if isinstance(content, str):
            content = content.encode('utf-8')

        self.status_code = status_code
        self.content = [content]
        self.header = list(header)
        self.cookies = {}

    def __iter__(self):
        return iter(self.content)


class Http404(Response):
    def __init__(self, content='404 Not Found'):
        super().__init__(status_code='404 Not Found', content=content)


class Http403(Response):
    def __init__(self):
        super().__init__(status_code='403 Forbidden', content='403 Forbidden')
