from .utils import *
from cdserver.utils import cookie
import cgi
import json
import urllib.parse


class FormData(object):
    def __init__(self, stream, content_length, boundary, chunk_size=64):
        self.stream = stream
        self.content_length = content_length
        self.boundary = b'--'+boundary.encode('ascii') if isinstance(boundary, str) else boundary
        self._boundary_pos = 0
        self._cache = b''
        self.read_length = 0
        self._files = {}
        self._post = []
        self.chunk_size = chunk_size
        self._value = b""
        self.load_data()


    def load_data(self):
        _line = True
        while _line:
            _line = self.chunk()

        self.stream.close()

    def put_value(self, value):
        if not self._value:
            return

        values = self._value.split(b'\r\n')
        opts = values[1].split(b';')
        print(values[1], opts)

    def _boundary(self, line):
        _line = self._cache + line
        while True:
            find = _line.find(self.boundary)
            if find > -1:
                self._value += _line[:find]
                _line = _line[find + len(self.boundary):]
                self.put_value(self._value)
                print(find, self._value, _line)
                self._value = b''
            else:
                break
        self._cache = _line

    def chunk(self):
        """

        :return: readed, b'content'
        """
        if self.read_length >= self.content_length:
            return b''

        size = self.chunk_size
        if self.read_length + self.chunk_size > self.content_length:
            size = self.content_length - self.read_length

        _line = self.stream.read(size)
        self.read_length += size
        self._boundary(_line)
        return _line




class Request(object):
    def __init__(self, environ):
        self.META = environ
        self.path = environ.get('PATH_INFO', '/')

    @property
    def method(self):
        return self.META['REQUEST_METHOD']

    @property
    def COOKIES(self):
        raw_cookie = get_str_from_wsgi(self.META, 'HTTP_COOKIE', '')
        return cookie.parse_cookie(raw_cookie)

    @property
    def POST(self):
        if hasattr(self, '_post_data'):
            return getattr(self, '_post_data')

        self.content_type, self.content_paras = cgi.parse_header(self.META.get('CONTENT_TYPE', ''))

        content_length = int(self.META.get('CONTENT_LENGTH') or 0)
        stream = self.META['wsgi.input']

        if self.content_type.lower() == 'application/json':
            body = stream.read(content_length)
            stream.close()
            self._post_data = json.loads(body.decode('utf-8')) if len(body) else {}
            return self._post_data

        print(self.content_type)
        return {}

    @property
    def GET(self):
        if hasattr(self, '_get_data'):
            return self._get_data

        query_string = self.META['QUERY_STRING']
        self._get_data = urllib.parse.parse_qs(query_string)
        return self._get_data



