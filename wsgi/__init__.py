from cdserver.utils.module_loading import import_string
from cdserver.core.router import UrlRouter
from .request import Request
from .response import Http404
import cdserver


class MiddlewareHandler(object):
    def __init__(self, middlewares):
        self.middleware_chain = []
        for _path in middlewares:
            print('load wsgi middleware', _path)
            middleware = import_string(_path)
            self.middleware_chain.append(middleware)

    def process_request(self, request):
        for middleware in self.middleware_chain:
            if hasattr(middleware, 'process_request'):
                middleware.process_request(request)

    def process_response(self, request, response):
        for middleware in self.middleware_chain:
            if hasattr(middleware, 'process_response'):
                middleware.process_response(request, response)


class WSGIHandler(object):
    def get_response(self, request):
        view, kwargs = self.url_router.get_url_view(request.path)
        if callable(view):
            return view(request, **kwargs)

        return Http404()

    def __init__(self, urls, settings):
        print('init')
        setattr(cdserver, 'settings', settings)
        self.middleware_handler = MiddlewareHandler(getattr(settings, 'MIDDLEWARES', []))
        self.url_router = UrlRouter(getattr(urls, 'urlpatterns', []))
        print(self.url_router.routers)

    def __call__(self, environ, start_response):
        request = Request(environ)
        self.middleware_handler.process_request(request)

        response = self.get_response(request)

        self.middleware_handler.process_response(request, response)

        header = [
            *response.header,
            *(('Set-Cookie', c.output(header='')) for c in response.cookies.values()),
        ]
        start_response(response.status_code, header)
        return response
