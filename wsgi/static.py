from .response import Response, Http404
import mimetypes
import os
import cdserver


def serve(request, path):
    """

    :param request:
    :return:
    """
    root = cdserver.settings.STATICFILES_DIR
    fpath = os.path.abspath(os.path.join(root, path))
    if os.path.isfile(fpath):
        content_type, encoding = mimetypes.guess_type(str(fpath))
        content_type = content_type or 'application/octet-stream'
        header = [('Content-Type', content_type)]
        if encoding:
            header.append(("Content-Encoding", encoding))

        with open(fpath, 'rb') as rf:
            content = rf.read()
            return Response(content=content, header=header,)
    else:
        return Http404()
