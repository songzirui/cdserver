from .response import JsonResponse, Response


class API(object):
    def __init__(self, request, *args, **kwargs):
        self.request = request
        self.args = args
        self.kwargs = kwargs

    def check_perm(self):
        return True

    @classmethod
    def as_view(cls):
        """Main entry point for a request-response process."""

        def view(request, *args, **kwargs):
            self = cls(request, *args, **kwargs)

            handler = getattr(self, request.method.lower(), None)
            if handler:
                if request.method in ['POST', 'GET', 'PUT', 'DELETE']:
                    if bool(self.check_perm()):
                        return handler(request, *args, **kwargs)
                    else:
                        return JsonResponse({'status': 'FAIL', 'errors': 'perms not allowed'})
                else:
                    return handler(request, *args, **kwargs)
            else:
                return Response(status_code='405 Method Not Allowed')

        return view

    def options(self, request, *args, **kwargs):
        """Handle responding to requests for the OPTIONS HTTP verb."""
        return Response()


class ListAPI(API):
    model = None
    fields = '__all__'
    limit = 10

    def get_result(self, request):

        skip = 0
        limit = self.limit
        if self.limit:
            if request.GET.get('limit') != 'all':
                try:
                    skip = int(request.GET.get('skip', 0))
                    limit = int(request.GET.get('limit', self.limit))
                except Exception as e:
                    return {"status": "FAIL", "errors": "无效的limit, skip参数 {}".format(e)}

        queryset = self.model.objects
        if self.fields is not '__all__':
            queryset = queryset.only(*self.fields)
        object_list = queryset.skip(skip).limit(limit)
        result = '{"status": "OK", "object_list": %s }'

        return result % object_list.to_json()

    def get(self, request, *args, **kwargs):
        try:
            return JsonResponse(self.get_result(request))
        except Exception as e:
            return JsonResponse({'status': 'FAIL', 'errors': str(e)})
