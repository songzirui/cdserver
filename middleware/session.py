from cdserver.utils import crypto


class SessionMiddleware(object):

    @classmethod
    def process_request(cls, request):
        pass

    @classmethod
    def process_response(cls, request, response):
        pass
