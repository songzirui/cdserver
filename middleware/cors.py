# 允许跨域


class CORSMiddleware(object):

    @classmethod
    def process_response(cls, request, response):
        response.header.append(('Access-Control-Allow-Origin', '*'))
        response.header.append(('Access-Control-Allow-Methods', '*'))
        response.header.append(('Access-Control-Allow-Headers', '*'))
