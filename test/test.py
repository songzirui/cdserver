import unittest
from cdserver.core.router import UrlRouter, include

def view():pass
def view1():pass
def view2():pass
def view3():pass
def view4():pass
def view5():pass
def view6():pass

class TestStringMethods(unittest.TestCase):

    def setUp(self):
        sub_patterns = [
            ('api/page/', view,),
        ]
        sub_patterns2 = [
            ('api/page/', view2),
            ('api/article/list/uuu/', view6),
        ]
        urlpatterns = [
            ('', view),
            ('api/article/list/', view1),
            ('api/article/', view2),
            ('api/article/(?P<name>\w+)/', view3),
            ('api/article/(?P<name>\w+)/ooo/', view4),
            ('static/(?P<path>.*)/', view5),
            ('admin', include(sub_patterns)),
            ('', include(sub_patterns2)),
        ]
        self.router = UrlRouter(urlpatterns)

    def test_router(self):
        urls = [
            ('', view),
            ('api/article/list/', view1),
            ('api/article/', view2),
            ('api/article/aaa/', view3),
            ('api/article/ddd/ooo/', view4),
            ('static/js/a.js/', view5),
            ('api/article/list/uuu/', view6),
        ]
        for url, v in urls:
            print('test url', url)
            _v, kw = self.router.get_url_view(url)
            self.assertEqual(v, _v)


if __name__ == '__main__':
    unittest.main()