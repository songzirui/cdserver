from cdserver.asgi.response import Response, Http404
import mimetypes
import os
import cdserver


def serve(request, path):
    """

    :param request:
    :return:
    """
    root = cdserver.settings.STATICFILES_DIR
    fpath = os.path.abspath(os.path.join(root, path))
    if os.path.isfile(fpath):
        content_type, encoding = mimetypes.guess_type(str(fpath))
        content_type = content_type or 'application/octet-stream'
        header = [(b'Content-Type', content_type.encode('utf-8'))]
        if encoding:
            header.append((b"Content-Encoding", encoding.encode('utf-8')))

        with open(fpath, 'rb') as rf:
            content = rf.read()
            return Response(content=content, header=header,)
    else:
        return Http404()
