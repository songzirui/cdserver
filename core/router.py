import re
from cdserver.utils.module_loading import import_string

__all__ = ['UrlRouter', 'include']


class UrlRouter:
    """ url = path/path/path/
        self.routers = {
            "": view,
            'api', {
                'news': view,
                '(?P<>)':
            }
        ]

    """
    def pop_path(self, url):
        res = ""
        i = 0
        for x in url:
            if x == '/':
                return res, url[i+1:]
            else:
                res += x
                i += 1
        return res, ""

    @classmethod
    def update_router(cls, router, router_added):
        for k, v in router_added.items():
            if k in router:
                cls.update_router(router[k], v)
            else:
                router[k] = v

    def add_path(self, url, view):
        router = self.routers
        url = url.strip('/')

        while url:
            path, url = self.pop_path(url)
            if path not in router:
                router[path] = dict()
            router = router[path]

        if isinstance(view, self.__class__):
            self.update_router(router, view.routers)
        else:
            router[""] = view

    def __init__(self, urls):
        self.routers = {}
        if isinstance(urls, str):
            urls = import_string(urls)

        for url, view in urls:
            self.add_path(url, view)

    @classmethod
    def match_url(cls, routers, url):
        # 正则匹配 router
        for path in routers:
            match = re.match("{}$".format(path), url)
            if match:
                return routers[path], match.groupdict()
        return {}, {}

    def get_url_view(self, url):
        # 获取url对应的view函数
        url = url.strip('/')
        routers = self.routers
        kwargs = {}

        while url:
            path, url = self.pop_path(url)
            if path in routers:
                # 直接找到
                routers = routers[path]
                continue

            # 最大匹配
            res, _kwargs = self.match_url(routers, '{}/{}'.format(path, url))
            if res:
                kwargs.update(_kwargs)
                routers = res
                url = ""
                continue

            # 局部匹配
            res, _kwargs = self.match_url(routers, path)
            if res:
                kwargs.update(_kwargs)
                routers = res
                continue

            # 404
            routers = {}

        return routers.get(url), kwargs


include = UrlRouter


class UrlRouter2:
    def add_path(self, url, view,):
        self.routers.append([url, view])

    def __init__(self, urls):
        self.routers = []
        for url, view in urls:
            self.add_path(re.compile(url), view)

    def get_url_view(self, url):
        # 获取url对应的view函数
        for path_re, view in self.routers:
            match = path_re.match(url)
            if match:
                return view, match.groupdict()
        return None, None
