import aioredis
import json
import functools
import time
import uuid


class RedisConnectionManager:
    """
    Async context manager for connections
    """

    def __init__(self, host):
        self.host = host

    async def __aenter__(self):
        self.conn = await aioredis.create_redis(**self.host)
        return self.conn

    async def __aexit__(self, exc_type, exc, tb):
        self.conn.close()


class RedisLayer(object):
    def __init__(self, host=None):
        if not host:
            host = {"address": ("localhost", 6379), 'db': 1}
        self.host = host

    async def new_channel(self):
        """
        Returns a new channel name that can be used by something in our
        process as a specific channel.
        """
        return uuid.uuid1().hex

    def get_channel_key(self, channel):
        return "c_{}".format(channel)

    async def send(self, channel, message):

        async with RedisConnectionManager(self.host) as connection:
            channel_key = self.get_channel_key(channel)
            print('send chanel', channel, channel_key, message)
            await connection.lpush(self.get_channel_key(channel), self.serialize(message))

    async def receive(self, channel):
        async with RedisConnectionManager(self.host) as connection:
            channel_key = self.get_channel_key(channel)
            print('wait chanel', channel_key)
            res = await connection.brpop(channel_key)
            return self.deserialize(res[1])

    def get_group_key(self, group):
        return "g_{}".format(group)

    async def group_add(self, group, channel):
        async with RedisConnectionManager(self.host) as connection:
            group_key = self.get_group_key(group)
            channel_key = self.get_channel_key(channel)
            await connection.zadd(group_key, time.time(), channel_key)

    async def group_discard(self, group, channel):
        async with RedisConnectionManager(self.host) as connection:
            group_key = self.get_group_key(group)
            channel_key = self.get_channel_key(channel)
            await connection.zrem(group_key, channel_key)

    async def group_send(self, group, message):
        async with RedisConnectionManager(self.host) as connection:
            group_key = self.get_group_key(group)
            _message = self.serialize(message)
            for channel_key in await connection.zrange(group_key, 0, -1):
                print('send chanel', channel_key, _message)
                await connection.lpush(channel_key, _message)

    def serialize(self, message):
        """
        Serializes message to a byte string.
        """
        return json.dumps(message)

    def deserialize(self, message):
        """
        Deserializes from a byte string.
        """
        return json.loads(message)
